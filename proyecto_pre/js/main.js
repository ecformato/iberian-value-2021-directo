import { scrollActiv, setRRSSLinks } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';
//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

//Ejecución
let viewportHeight = window.innerHeight;
let logoFooter = document.getElementById("logoFooter"); //Patrocinadores
let sharing = document.getElementById("sharing");

setRRSSLinks();

window.addEventListener('scroll', function(){
	scrollActiv();
	
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible")
	} else {
		sharing.classList.remove("visible")
	}

	if (isMobile()) {
		if (isElementInViewport(logoFooter)) {
			sharing.classList.remove("visible")
		}
	}
});